var express = require('express');
var path = require('path');
var events = require('./eventsController');
var users = require('./userController');

var app = express();
var rootPath = path.normalize(__dirname + '/../');
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static(rootPath + '/app'));

app.get('/data/event/', events.getIDAsync);
app.get('/data/event/:id', events.get);
app.post('/data/event/:id', events.save)
app.post('/data/user/:userName', users.save)

app.listen(8000);
console.log("Server started at port 8000....");


