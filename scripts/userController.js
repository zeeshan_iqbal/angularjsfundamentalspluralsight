var fs = require('fs');

module.exports.get = function (req, res) {
    var user = fs.readFileSync('app/data/user/' + req.params.userName + '.json', 'utf8');
    res.setHeader('Content-Type', 'application/json');
    return res.send(user);
};

module.exports.save = function (req, res) {
    var user = req.body;
    fs.writeFileSync('app/data/user/' + req.params.userName + '.json', JSON.stringify(user));
    return res.send(user);
}