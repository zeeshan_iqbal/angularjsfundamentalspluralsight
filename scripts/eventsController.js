var fs = require('fs');

function readDirectoryAsync(res) {
    fs.readdir('app/data/event/', function (error, files) {
       var id = getGreatestId(files);
       res.send({id: ++id});
    });
}

function getGreatestId(files) {
    var ids = [];
   files.forEach(function(element) {
       ids.push(parseInt(element.split('.')[0], 10));
   }, this);
   return ids.pop();
}

module.exports.get = function (req, res) {
    var event = fs.readFileSync('app/data/event/' + req.params.id + '.json', 'utf8');
    res.setHeader('Content-Type', 'application/json');
    return res.send(event);
};

module.exports.save = function (req, res) {
    var event = req.body;
    fs.writeFileSync('app/data/event/' + req.params.id + '.json', JSON.stringify(event));
    return res.send(event);
}

module.exports.getIDAsync = function (req, res) {
    readDirectoryAsync(res);
}