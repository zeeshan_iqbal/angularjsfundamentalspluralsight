'use strict';

eventsApp.factory('$exceptionHandler', function ($log) {
    
    return function (exception) {
        $log.error("Execption is handled: " + exception.message);
    }
    
});