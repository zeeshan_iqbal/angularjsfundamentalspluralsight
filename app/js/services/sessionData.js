'use strict';

eventsApp.factory('sessionData', function ($cookieStore) {
    function getUserSessionUpvote(userName) {
        return $cookieStore.get(userName);
    }
    function updateSessionUpvote(userName, eventId) {
        var cookie = getUserSessionUpvote(userName);
        if(!cookie){
            $cookieStore.put(userName, {});
            cookie = getUserSessionUpvote(userName)
        }
        if (!cookie[eventId]) {
            cookie[eventId] = true;
            $cookieStore.put(userName, cookie);
            return true;
        }
        return false;

    }
    return {
        update: function (eventId, userName) {
            userName = userName || 'zeeshaniqbal';            
            return updateSessionUpvote(userName, eventId);
        }
    }
})