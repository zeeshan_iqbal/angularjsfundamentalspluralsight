'use strict';

eventsApp.factory('userData', function ($resource) {
    var userResource = $resource('/data/user/:userName', {userName: '@userName'});
    return {
        save: function (user) {
            return userResource.save(user);
        },
        get: function (userName) {
            return userResource.get(userName);
        }
    }
})