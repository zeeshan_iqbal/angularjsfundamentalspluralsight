'use strict';

eventsApp.factory('eventData', function ($resource) {
    var resource = $resource("/data/event/:id", { id: '@id' });
    var fileServive = $resource("/data/event", null, { getIDAsync: { method: 'GET'} });
    return {
        getEvent: function () {
            return resource.get({ id: 1 });
        },
        saveEvent: function (event, successCB, failureCB) {
            return resource.save(event);
        },
        getGreatestEventID: function (callback) {
             return fileServive.getIDAsync();
        }

    };
});