'use strict';

eventsApp.controller("EventController", function EventController($scope, eventData, $log, $anchorScroll, sessionData) {
    $scope.sortorder = 'name';
    $scope.event = eventData.getEvent()
        .$promise
        .then(function (event) {
            $scope.event = event;
            console.log(event);
        })
        .catch(function (response) {
            $log.error(response);
        });

    $scope.upVoteSession = function (session) {
        if (sessionData.update(session.id)) {
            console.log("updated");
            session.upVoteCount++;
            return;
        }
        console.log("You can update only once!");
    };

    $scope.downVoteSession = function (session) {
        session.upVoteCount--;
    };

    $scope.scrollToSession = function () {
        $anchorScroll();
    }
});
