'use strict';

eventsApp.controller('TimeoutSampleController', function TimeoutSampleController($scope, $timeout) {
    //$parse service demo
    var promise = $timeout(function (params) {
        $scope.name = "John Doe";
    }, 3000);
    
    $scope.cancel = function () {
        $timeout.cancel(promise);
    }
})