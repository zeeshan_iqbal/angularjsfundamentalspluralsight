'use strict';

eventsApp.controller('EditEventController', function EditEventController($scope, eventData) {
    $scope.saveEvent = function (event, newEventForm) {
        if (!newEventForm.$invalid) {
            eventData.getGreatestEventID()
                .$promise
                .then(function (res) {
                    event.id = res.id;
                    eventData.saveEvent(event)
                        .$promise
                        .then(function (response) { console.log('success', response) })
                        .catch(function (response) { console.log('failure', response) });
                })
                .catch(function (response) {
                    console.error(response);
                });
        }
    };

    $scope.cancelEvent = function () {
        window.location = '/EventDetails.html';
    }

    function getGreatestEventId() {
        eventData.get

    }

});