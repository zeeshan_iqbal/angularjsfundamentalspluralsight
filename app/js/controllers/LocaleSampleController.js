'use strict';

eventsApp.controller('LocaleSampleController', function LocaleSampleController($scope, $locale) {
    //$parse service demo
    $scope.myDate = new Date();
    $scope.myFormat = $locale.DATETIME_FORMATS.fullDate;
    
    throw 'Error Message';
})