'use strict';

eventsApp.controller('CompileSampleController', function CompileSampleController($scope, $compile, $parse) {
    //$parse service demo
    
    var fn = $parse('1 + 2');
    console.log(fn());
    
    var getter = $parse('event.name'),
    context1 = {event: {name: 'AngularJs Bootcamp'}},
    context2 = {event: {name: 'Code Camp'}};
    
    console.log(getter(context1));
    console.log(getter(context2));
    console.log(getter(context2, context1));
    
    var setter = getter.assign;
    setter(context1, "AngularJs Fundamentals");
    console.log(getter(context1));
    
    // end $parse service demo
  $scope.appendDivToElement = function (markup) {
      $compile(markup)($scope).appendTo(angular.element('#appendHere'));
  }  
})